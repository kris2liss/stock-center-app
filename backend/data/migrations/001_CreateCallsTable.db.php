<?php

$tableName = 'items';

$database->create($tableName, [
    'id' => [ "INT", "NOT NULL", "AUTO_INCREMENT", "PRIMARY KEY"],
    'title' => ['VARCHAR(256)', 'NOT NULL'],
    'price' => ['FLOAT', 'NOT NULL'],
    'timestamp' => ['DATETIME', 'NOT NULL'],
]);
