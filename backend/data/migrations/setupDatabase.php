<?php

use Medoo\Medoo;

$DS = DIRECTORY_SEPARATOR;

require __DIR__ . $DS . '..' . $DS . '..' . $DS . 'vendor' . $DS . 'autoload.php';


$database = new Medoo([
    'type' => 'mysql',
    'host' => 'mysql',
    'database' => 'mysql',
    'username' => 'root',
    'password' => 'mysql',
]);

$files = glob(__DIR__ . '/*.db.php');

foreach ($files as $file) {
    require_once($file);   
}
