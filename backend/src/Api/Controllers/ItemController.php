<?php

namespace Api\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Symfony\Component\Validator\Validation;
use Core\UseCases\StoreItemUseCase;
use Core\UseCases\GetItemListUseCase;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ItemController
{
    private ValidatorInterface $validator;

    public function __construct(
        private StoreItemUseCase $StoreItemUseCase,
        private GetItemListUseCase $GetItemListUseCase
    ) {
        $this->validator = Validation::createValidator();
    }

    public function get(Request $request, Response $response)
    {
        $data = $this->GetItemListUseCase->run();
        return $this->sendJsonResponse($response, $data);
    }

    public function post(Request $request, Response $response)
    {
        $success = true;
        $params = (array)$request->getParsedBody();
        $result = $this->validateParams($params);
        if (count($result) === 0) {
            $result = $this->StoreItemUseCase->run($params['title'], (float) $params['price'], $params['timestamp']);
        } else {
            $success = false;
        }


        return $this->sendJsonResponse($response, $result, $success);
    }

    private function validateParams($params)
    {
        $result = [];

        $constraints = new Assert\Collection([
            'title' => [
                new Assert\NotBlank(),
                new Assert\Length([
                    'min' => 5,
                    'max' => 50,
                ])
            ],
            'price' => [
                new Assert\NotBlank(),
                new Assert\Positive(),
                new Assert\Regex([
                    'pattern' => '/^\d+\.\d\d$/',
                    'message' => 'Enter price like 10.00',
                ])
            ],
            'timestamp' => [
                new Assert\NotBlank(),
                new Assert\Regex([
                    'pattern' => '/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]) (0[0-9]|1[0-9]|2[1-4]):(0[0-9]|[1-5][0-9]):(0[0-9]|[1-5][0-9]))$/',
                    'message' => 'Enter date and time like 2000-01-01 00:00:00',
                ])
            ],
        ]);

        $violations = $this->validator->validate($params, $constraints);
        
        if (0 !== count($violations)) {
            foreach ($violations as $violation) {
                // getPropertyPath() return [propertyName], need strip []
                $propertyName = substr($violation->getPropertyPath(), 1, -1);
                $result[$propertyName] = $violation->getMessage();
            }
        }

        return $result ;
    }

    // response in JSend specification format
    private function sendJsonResponse(Response $response, $data = null, $success = true): Response
    {
        $data = ['status' => $success ? 'success' : 'fail', 'data' => $data];
        $payload = json_encode($data);
        $response->getBody()->write($payload);

        return $response
            ->withHeader('Content-Type', 'application/json');
    }
}
