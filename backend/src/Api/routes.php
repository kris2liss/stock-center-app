<?php

use Api\Controllers\ItemController;

$app->get('/api/items', [ItemController::class, 'get']);
$app->post('/api/item', [ItemController::class, 'post']);

$app->run();
