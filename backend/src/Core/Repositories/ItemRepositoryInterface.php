<?php

namespace Core\Repositories;

use Core\Entities\Item;

interface ItemRepositoryInterface
{
    public function add(Item $item): int;
    public function getItemList();
}
