<?php

namespace Core\UseCases;

use Core\Entities\Item;
use Core\Repositories\ItemRepositoryInterface;

class GetItemListUseCase
{
    public function __construct(
        private ItemRepositoryInterface $ItemRepository
    ) {
    }

    public function run(): array
    {
        return $this->ItemRepository->getItemList();
    }
}
