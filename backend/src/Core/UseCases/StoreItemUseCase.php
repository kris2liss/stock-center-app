<?php

namespace Core\UseCases;

use Core\Entities\Item;
use Core\Repositories\ItemRepositoryInterface;

class StoreItemUseCase
{
    public function __construct(
        private ItemRepositoryInterface $ItemRepository,
    ) {
    }

    public function run(string $title, float $price, string $timestamp): Item
    {
        $item = new Item(null, $title, $price,  $timestamp);
        $id = $this->ItemRepository->add($item);

        $item->setId($id);
        return $item;
    }
}
