<?php

namespace Persistence;

use Core\Repositories\ItemRepositoryInterface;
use Core\Entities\Item;

class ItemRepository implements ItemRepositoryInterface
{
    private  $database;
    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database->getInstance();
    }

    public function add(Item $item): int
    {
        $this->database->insert('items', [
            'id' => null,
            'title' => $item->getTitle(),
            'price' => $item->getPrice(),
            'timestamp' => $item->getTimestamp(),
        ]);

        return $this->database->id();
    }

    public function getItemList()
    {
        $items = $this->database->select('items', '*');

        $result = [];
        foreach ($items as $fields) {
            $result[] = $this->getItemEntity($fields);
        }
        return $result;
    }

    private function getItemEntity($fields): Item
    {
        return new Item(
            $fields['id'],
            $fields['title'],
            $fields['price'],
            $fields['timestamp'],
        );
    }
}
