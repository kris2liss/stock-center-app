<?php

namespace Persistence;

use Medoo\Medoo;

class MySqlDatabase implements DatabaseInterface
{
    public function getInstance()
    {
        $database = new Medoo([
            'type' => 'mysql',
            'host' => 'mysql',
            'database' => 'mysql',
            'username' => 'root',
            'password' => 'mysql',
        ]);

        return $database;
    }
}
