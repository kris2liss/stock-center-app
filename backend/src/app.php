<?php

declare(strict_types=1);

use DI\ContainerBuilder;

require __DIR__ . DS . '..' . DS . 'vendor' . DS . 'autoload.php';

$containerBuilder = new ContainerBuilder();
$containerBuilder->addDefinitions(__DIR__ . DS . 'di.php');
$container = $containerBuilder->build();

$app = \DI\Bridge\Slim\Bridge::create($container);

$app->addBodyParsingMiddleware();

require __DIR__ . DS . 'Api' . DS . 'routes.php';
