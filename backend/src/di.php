<?php

use Core\Repositories\ItemRepositoryInterface;
use Persistence\ItemRepository;
use Persistence\DatabaseInterface;
use Persistence\MySqlDatabase;

return [
    DatabaseInterface::class => DI\create(MySqlDatabase::class),
    ItemRepositoryInterface::class => DI\create(ItemRepository::class)
        ->constructor(DI\get(DatabaseInterface::class)),
    
];
