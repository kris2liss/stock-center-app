import m from 'mithril'

import './resources/styles.sass'

import layout from './layouts'

m.mount(document.body, layout)
