import m from 'mithril'
import ModalDialog from './ModalDialog'


module.exports = {
    view: vnode => [
      m('footer.footer',
        m('.container',
          m('.level',
            m('.level-item.has-text-centered',
              m('div', 
                m('button.button', {onclick: () => ModalDialog.showForm()}, 'New item')
              )
            )
          )
        )
    ),
    m(ModalDialog, { active: vnode.state.modalActive })
  ]
}
