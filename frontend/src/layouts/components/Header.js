import m from 'mithril'

module.exports = {
    view: vnode => [ 
        m('nav.navbar.is-primary', [
        m('.container', [
        m('.navbar-brand', [
            m('a.navbar-item', { href: '/', title: 'Home' },
                m('.header-title', 'Stock Center')
            ),
        m('.navbar-item', 'Items in stock'),
            m('a.navbar-burger[data-target="navbar-menu"]', { role: 'button', 'aria-label': 'menu', 'aria-expanded': 'false' }, [
            m('span'),
            m('span'),
            m('span')
            ])
        ])
        ])
  ])
    ]}
