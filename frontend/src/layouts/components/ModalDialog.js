import m from 'mithril'
import Item from '../../models/Item'

function resetFormErrors() {
    ModalForm.errors.title = false
    ModalForm.errors.price = false
    ModalForm.errors.timestamp = false
}

const AddButton = {
    loading: false,
    submit () {
        this.loading = true
        Item.addItem(ModalForm.item).then(result => {
            this.loading = false;

            if(result.status === 'success') {
                ModalForm.hide()
                resetFormErrors()
                Item.loadList()
            } else {
                resetFormErrors()
                Object.entries(result.data).forEach((el, index) => {
                    ModalForm.errors[el[0]] = el[1]
                })
            }
        })
    },

    view () {
    return m('button.button.is-primary',
        {
        class: this.loading ? 'is-loading' : '',
        onclick: () => AddButton.submit()
        },
        'Add'
    )
    }
}

const ModalForm = {
    class: '',
    isEmpty: () => ModalForm.item.id === undefined,
    item: {},
    errors: {
        title: false,
        price: false,
        timestamp: false
    },

    hide () {
        ModalForm.class = ''
    },

    view ()  {
        return m('.modal', {'class': this.class},
            m('.modal-background'),
            m('.modal-card',
                m('header.modal-card-head', 
                    m('p.modal-card-title', 'New item'),
                    m('button', {class: 'delete', onclick: () => this.hide()})
                ),
                m('section.modal-card-body', 
                    m('form',
                        m('.field',
                            m('label.label', 'Title'),
                            m('.control',
                                m('input.input', {type: 'text', placeholder: 'Enter title of item', class: ModalForm.errors.title && 'is-danger', oninput: e => { ModalForm.item.title = e.target.value }, value: ModalForm.item.title }),
                                m('span.help', {class: ModalForm.errors.title && 'is-danger'}, ModalForm.errors.title)
                            )
                        ),
                        m('.field',
                            m('label.label', 'Price'),
                            m('.control',
                                m('input.input', {type: 'text', placeholder: 'Enter price of item', class: ModalForm.errors.price && 'is-danger', oninput: e => { ModalForm.item.price = e.target.value }, value: ModalForm.item.price }),
                                m('span.help', {class: ModalForm.errors.price && 'is-danger'}, ModalForm.errors.price)
                            )
                        ),
                        m('.field',
                            m('label.label', 'Date and time'),
                            m('.control',
                                m('input.input', {type: 'text', placeholder: 'Enter date and time of item', class: ModalForm.errors.timestamp && 'is-danger', oninput: e => { ModalForm.item.timestamp = e.target.value }, value: ModalForm.item.timestamp }),
                                m('span.help', {class: ModalForm.errors.timestamp && 'is-danger'}, ModalForm.errors.timestamp)
                            )
                        )
                    )
                ),
                m('footer.modal-card-foot',
                    m('button.button', {onclick: () => this.hide()}, 'Close'),
                    m(AddButton)
                )
            )
        )
    },

    showForm () { ModalForm.item = {}; ModalForm.class = 'is-active' },
}


module.exports = ModalForm
