import m from 'mithril'

import Header from './components/Header.js'
import Content from '../pages/index.js'
import Footer from './components/Footer.js'

module.exports = {
      view: vnode => m('.main',
        m(Header),
            m('.section',
            m('.container',
                m(Content)
            )
            ),
        m(Footer)
      )
    }

  