import m from 'mithril'

const Item = {
    list: [],
    loadList: function() {
        return m.request({
            method: "GET",
            url: "/api/items"
        })
        .then(function(result) {
            Item.list = result.data
        })
    },
    addItem: function (item) {
        return m.request({
            method: "POST",
            url: "/api/item",
            body: item
        })
        .then(function(result) {
            return result
        })
    }
}

module.exports = Item
