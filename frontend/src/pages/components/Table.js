import m from 'mithril'
import Item from '../../models/Item'

const Header = {
    view: vnode => m('tr',
            m('th', '#'),
            m('th', 'Title'),
            m('th', 'Price, USD'),
            m('th', 'Date and time')
        )
}

const TableBody = {
    oninit: Item.loadList,
    view: vnode => {
        return Item.list.map(function(item) {
            return m('tr',
                m('td', item.id),
                m('td', item.title),
                m('td', item.price),
                m('td', item.timestamp)
            )
        })
    }
}

module.exports = {
    view: vnode => [
        m('table.table',
            [
                m(Header),
                m(TableBody)
            ]
        )
    ]
}
