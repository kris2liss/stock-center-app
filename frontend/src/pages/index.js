import m from 'mithril'
import Table from './components/Table'

module.exports = {
    view: function() {
        return m(Table)
    }
}
